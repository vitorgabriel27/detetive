import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LostComponent } from './components/lost/lost.component';
import { FindComponent } from './components/find/find.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { FilterComponent } from './components/filter/filter.component';
import { FoundItemComponent } from './components/found-item/found-item.component';
import { LostItemComponent } from './components/lost-item/lost-item.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'lost', component: LostComponent},
  {path: 'find', component: FindComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'categories', component: CategoriesComponent},
  {path: 'filter', component: FilterComponent},
  {path: 'found-item', component: FoundItemComponent},
  {path: 'lost-item', component: LostItemComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],


exports: [RouterModule]
})
export class AppRoutingModule { }
