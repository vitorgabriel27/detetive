import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconsModule } from './icons/icons.module';
import { AvatarModule } from 'ngx-avatar';
import { CommonModule } from '@angular/common'; 
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from "@angular/material/icon";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTabsModule } from '@angular/material/tabs';
import { HomeComponent } from './components/home/home.component';
import { FindComponent } from './components/find/find.component';
import { LostComponent } from './components/lost/lost.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { FilterComponent } from './components/filter/filter.component';
import { FoundItemComponent } from './components/found-item/found-item.component';
import { LostItemComponent } from './components/lost-item/lost-item.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FindComponent,
    LostComponent,
    ProfileComponent,
    TabsComponent,
    CategoriesComponent,
    FilterComponent,
    FoundItemComponent,
    LostItemComponent,
 
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    NgbModule,
    MatTabsModule,
    BrowserAnimationsModule,
    IconsModule,
    AvatarModule,
    CommonModule,
    HttpClientModule,
    MatIconModule,      

  ],
  providers: [],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
