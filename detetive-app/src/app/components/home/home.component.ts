import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public list = [
    {
      icon: 'wallet',
      categorie:'Carteira',
    },
    {
      icon: 'smartphone',
      categorie:'Celular',
    },
    {
      icon: 'headphone',
      categorie:'Fones',
    },
    {
      icon: 'dollar',
      categorie:'Dinheiro',
    },
    {
      icon: 'clock',
      categorie:'Relógios',
    },
  
    


   
    
  ];

}
