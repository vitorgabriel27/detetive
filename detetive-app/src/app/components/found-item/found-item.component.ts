import { Component, OnInit } from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-found-item',
  templateUrl: './found-item.component.html',
  styleUrls: ['./found-item.component.scss']
})
export class FoundItemComponent implements OnInit {

  showNavigationArrows = true;
  showNavigationIndicators = false;
  images = [1055, 194, 368].map((n) => `https://picsum.photos/id/${n}/900/315`);

  constructor(config: NgbCarouselConfig) {
    
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }

  ngOnInit(): void {
  }

}
