import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  

  public items = [
    {
      color: '--c-medium',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local achado',
      path: 'lost-item',
    },
    {
      color: '--c-tertiary',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local perdido',
      path: 'found-item',
    },
    {
      color: '--c-medium',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local achado',
      path: 'lost-item',
    },
    {
      color: '--c-tertiary',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local perdido',
      path: 'found-item',
    },
    {
      color: '--c-medium',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local achado',
      path: 'lost-item',
    },
    {
      color: '--c-tertiary',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local perdido',
      path: 'found-item',
    },
    {
      color: '--c-medium',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local achado',
      path: 'lost-item',
    },
    {
      color: '--c-tertiary',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local perdido',
      path: 'found-item',
    },
    {
      color: '--c-medium',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local achado',
      path: 'lost-item',
    },
    {
      color: '--c-tertiary',
      name:'Nome do perdido',
      date: '00/00/0000',
      locale: 'Local perdido',
      path: 'found-item',
    },
    
  ];

  public filters = [
    {
      filter: 'Filtro',
    },
    {
      filter: '',
    },
    {
      filter: 'Celular',
    }
  ];
}
