import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public list = [
    {
      title: 'Editar perfil',
      icon: 'profile',
    },
    {
      title: 'Configurações do app',
      icon: 'setting',
    },
    {
      title: 'Sair',
      icon: 'logout',
    },
  ]
}
