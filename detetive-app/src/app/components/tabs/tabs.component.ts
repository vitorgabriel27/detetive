import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  ngOnInit(): void {
  }

  title = 'detetive-app';

  background = '#000'; 

  constructor (public route: ActivatedRoute){}

  public links = [
    {
    path: 'home' , label: 'Inicio', icon:'home'
  },
  {
    path: 'find' , label: 'Achar',icon:'search'
  },
  {
    path: 'lost' , label: 'Perdeu?',icon:'emoji-sad'
  },
  {
    path: 'profile' , label: 'Perfil',icon:'profile'
  },
];

}
