import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public list = [
    {
      title: 'Celulares',
      icon: 'smartphone',
    },
    {
      title: 'Fones de ouvido',
      icon: 'headphone',
    },
    {
      title: 'Relógios',
      icon: 'clock',
    },
    {
      title: 'Carteira',
      icon: 'wallet',
    },
    {
      title: 'Dinheiro',
      icon: 'dollar',
    },
    {
      title: 'Videogames',
      icon: 'game',
    },
    
  
  ]
}
