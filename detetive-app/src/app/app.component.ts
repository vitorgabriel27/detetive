import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor (
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon(
      "profile",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/profile-icon.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "dollar",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/dollar-circle.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "emoji-sad",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/emoji-sad.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "game",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/game.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "headphone",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/headphone.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "home",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/home-2.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "key",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/key.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "logout",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/logout.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "search",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/search-normal.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "setting",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/setting-2.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "simcard",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/simcard.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "ticket",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/ticket.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "smartphone",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/mobile.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "wallet",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/empty-wallet.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "clock",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/watch.svg"),
      
    );
    this.matIconRegistry.addSvgIcon(
      "camera",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/camera.svg"),
      
    );
  }
}
