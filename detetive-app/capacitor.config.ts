import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.socionerd.detetiveapp',
  appName: 'detetive-app',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
